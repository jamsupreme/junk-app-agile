﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JunkApp.Business;
using Xbehave;
using Xunit.Should;

namespace JunkApp.Tests
{
    /// <summary>
    /// Tests the ultimate computer
    /// </summary>
    public class TestUltimateComputer
    {
        [Scenario]
        public void TestAdd()
        {
            Random r = new Random();
            int x = r.Next(1, 100);
            int y = r.Next(1, 100);
            int z = 0;
            ("Given any number x:" + x + " and y:" + y).f(() => { });
            "When I add them".f(() =>
            {
                z = UltimateComputer.Add(x, y);
            });
            ("Then the result should be " + (x + y)).f(() =>
            {
                z.ShouldNotBe(0);
                z.ShouldBe(x + y);
            });
        }
    }
}
