﻿namespace JunkApp.Business
{
    public static class UltimateComputer
    {
        public static int ComputeFactorial(int x)
        {
            return x * ComputeFactorial(x - 1);
        }

        public static int Add(int x, int y)
        {
            return x + y;
        }
    }
}
