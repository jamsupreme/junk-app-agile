﻿using System.Web.Http;

namespace JunkApp.Controllers.Api
{
    public class RootController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get()
        {
            return this.Ok("You have reached the root");
        }
    }
}
