﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace JunkApp
{
    public static class WebApiConfig
    {
        //note to self - build server AMI is .NET 3.5 (based on provided bamboo img)
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
